export const API_BASE_URL = 'https://randomuser.me/';

export const API_ENDPOINTS = {
  GET_LIST: 'api/?results=10',
};
